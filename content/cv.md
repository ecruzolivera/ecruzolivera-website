+++
title= "CV"
date = 2018-11-22
+++

# Ernesto Cruz Olivera

[ernesto@ecruzolivera.tech](mailto:ernesto@ecruzolivera.tech?subject=Reaching%20Out&body=How%20are%20you)  
+52 1 612 229 2758

## Skills

|                  |            |                           |
| :--------------: | :--------: | :-----------------------: |
|      Linux       |   C/C++    |    Project Management     |
|  Embedded Linux  |   Python   |            Git            |
| Microcontrollers |    Html    | Digital Signal Processing |
|       RTOS       | Javascript |            Qt             |
|    FPGA/VHDL     |  CSS/SASS  |           React           |

## Education

---

> September 2017 – July 2018 : Master in **Electronic Systems Engineering** \
> _UC3M Campus Leganes_ \
> _Madrid, Spain_

---

> March – April 2016 : Postgraduate Course: **Production Management** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> October – December 2015 : Postgraduate Course: **Embedded Digital Systems** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> September 2015 : Postgraduate Course: **Operative Systems** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> April - June 2015 : Postgraduate Course: **Programming in Windows** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> March - April 2015 : Postgraduate Course: **Microcontrollers** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> January - April 2015 : Postgraduate Course: **Object Oriented Programming** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> 2011 - 2012 : **Diploma in Neuroscience** \
> _Cuban Neuroscience Center_ \
> _Havana, Cuba_

---

> 2006 - 2011 : **Engineering in Telecommunications and Electronics** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

## Job Experience

---

> 2018 - 2019 : **Freelancer**

---

> 2011 - 2017 : **Embedded Systems Engineer** \
> _Cuban Neuroscience Center_ \
> _Havana, Cuba_

---

> 2014 : **Junior College Teacher** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

> 2007-2011 : **Assistant Teacher** \
> _Technological University of Havana_ \
> _Havana, Cuba_

---

## Languages

**Spanish**: Mother tongue \
**English**: Listening (excellent), Reading (excellent), Speaking (Average), Writing (Average).
