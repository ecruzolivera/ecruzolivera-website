# Portfolio

- [FrankeNews](#frankenews)
- [Pomodoro Timer](#pomodoro-timer)
- [Old personal website](#old-personal-website)
- [eHSM Library](#ehsm)

## FrankeNews

![FrankeNews screenshot](/images/frankenews.png)

A news aggregator that pulls info from several 3rd party APIs:

- [x] [News API](https://newsapi.org/)
- [x] [Bing News Search](https://azure.microsoft.com/en-us/services/cognitive-services/bing-news-search-api/)

Where to find it:

- **url:** [frankenews.ecruzolivera.tech](https://frankenews.ecruzolivera.tech/) \
- **repository:** [https://gitlab.com/ecruzolivera/frankenews](https://gitlab.com/ecruzolivera/frankenews)

Technologies used:

- [React](https://reactjs.org/)
- [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
- [Google Material UI](https://material-ui.com/)

## Pomodoro Timer

![Pomodoro web app screenshot](/images/pomodoro_screenshot.png)

This webapp implements the [pomodoro timer technique](https://en.wikipedia.org/wiki/Pomodoro_Technique).

Its a [PWA](https://developers.google.com/web/progressive-web-apps/) (Progressive Web Application), _works offline!!!!!_

Where to find it:

- **url:** [pomodoro.ecruzolivera.tech](https://pomodoro.ecruzolivera.tech) \
- **repository:** [https://gitlab.com/ecruzolivera/pomodoro-timer-webapp](https://gitlab.com/ecruzolivera/pomodoro-timer-webapp)

Technologies used:

- [React](https://reactjs.org/)
- [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
- [SASS](https://sass-lang.com/)

## Old personal website

![old site screenshot](/images/oldsite.png)

This is a static website implemented using NextJs.

Where to find it:

- **url:** [ecruzolivera-old.ecruzolivera.tech](https://ecruzolivera-old.ecruzolivera.tech)
- **repository:** [https://gitlab.com/ecruzolivera/ecruzolivera-website-old](https://gitlab.com/ecruzolivera/ecruzolivera-website-old/)

Technologies used:

- [React](https://reactjs.org/)
- [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
- [SASS](https://sass-lang.com/)
- [NextJs](https://nextjs.org/)

## eHSM

eHSM is an acronym for **e**mbedded **H**ierarchical **S**tate **M**achine. This C++ library provides a class that implements the State Pattern, and a collection of utility classes.

Provides:

- Template Based Ring Buffer, Array and DMA Buffer Containers, with static memory allocation using [this pattern](https://youtu.be/TYqbgvHfxjM?t=1999)
- Delegate class and Qt-ish Signal class, sort of [this](http://doc.qt.io/archives/qt-4.8/signalsandslots.html), but way more simplified.
- [Hierarchical State Machine](https://barrgroup.com/Embedded-Systems/How-To/Introduction-Hierarchical-State-Machines)

Tested with gcc for x82 and ARM architectures, but should work in any architecture with a decent C++03 support.

Where to find it:

- **repository:** [https://gitlab.com/ecruzolivera/eHSM](https://gitlab.com/ecruzolivera/eHSM/)

Technologies used:

- [C++03](https://en.cppreference.com/w/)
- [Google Test](https://github.com/google/googletest)
