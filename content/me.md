# About Me

I'm a Developer with 7 years of experience designing and implementing software for Embedded Systems.

Colleagues know me as a highly creative engineer who can always be trusted to come up with a new approach. I spend a lot of time understanding the project requirements before start designing the solution. I can (and often do) work well alone, but I’m at my best collaborating with others.

My areas of interest are not only restricted to Embedded Systems, they also cover the fields of Digital Signal Processing and Web Development.
